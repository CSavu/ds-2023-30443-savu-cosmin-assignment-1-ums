package com.ums.services;

import com.ums.entities.User;
import com.ums.exceptions.InvalidPropertyException;
import com.ums.rabbitmq.UserUpdateProducer;
import com.ums.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserUpdateProducer userUpdateProducer;

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(InvalidPropertyException::new);
    }

    public User createUser(User user) {
        if (userRepository.existsByUsername(user.getUsername())) throw new InvalidPropertyException();

        return userRepository.save(
                new User(user.getUsername(), passwordEncoder.encode(user.getPassword()), user.getRole()));
    }

    @Transactional
    public Boolean deleteUser(Integer userId) {
        try {
            userRepository.deleteById(userId);
            userUpdateProducer.publishUserDeletionMessage(Integer.toString(userId));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public User updateUser(User user) {
        if (userRepository.existsByUsername(user.getUsername())) {
            User oldUser = userRepository.findByUsername(user.getUsername()).orElseThrow(InvalidPropertyException::new);
            return userRepository.save(
                    new User(oldUser.getId(), user.getUsername(), oldUser.getPassword(), user.getRole()));
        }
        throw new InvalidPropertyException();
    }

    public User getById(Integer userId) {
        return userRepository.findById(userId).orElseThrow(InvalidPropertyException::new);
    }
}
