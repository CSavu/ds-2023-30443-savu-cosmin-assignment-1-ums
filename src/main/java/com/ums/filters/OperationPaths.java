package com.ums.filters;

public enum OperationPaths {
    AUTHENTICATE("/authenticate");

    private String path;

    OperationPaths(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
