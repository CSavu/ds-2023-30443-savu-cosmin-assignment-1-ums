package com.ums.configs;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    @Value("${ums.rabbitmq.caching.connection.factory.hostname}")
    private String hostname;

    @Value("${ums.rabbitmq.user.exchange}")
    private String userExchange;
    @Value("${ums.rabbitmq.user.delete.queue}")
    private String userDeleteQueue;
    @Value("${ums.rabbitmq.user.delete.routing_key}")
    private String userDeleteRoutingKey;

    @Bean
    public CachingConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(hostname);
    }

    @Bean
    public RabbitAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        return new RabbitTemplate(connectionFactory());
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(userDeleteQueue())
                .to(userExchangeTopic())
                .with(userDeleteRoutingKey);
    }

    @Bean
    public TopicExchange userExchangeTopic() {
        return new TopicExchange(userExchange);
    }

    @Bean
    public Queue userDeleteQueue() {
        return new Queue(userDeleteQueue);
    }
}
