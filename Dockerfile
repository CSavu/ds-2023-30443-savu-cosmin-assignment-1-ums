FROM openjdk:17.0.1
VOLUME /tmp
EXPOSE 8443
ARG JAR_FILE
COPY ${JAR_FILE} ums.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","/ums.jar"]